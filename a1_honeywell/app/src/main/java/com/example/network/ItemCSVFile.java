package com.example.network;


import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

public class ItemCSVFile {
    InputStream inputStream;
    Date d;
    Double c;


    public ItemCSVFile(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public LinkedList read() {

        LinkedList<ItemModel> resultList = new LinkedList<ItemModel>();
        resultList.clear();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                String[] row = csvLine.split(";");
                Double ca = stringToDouble(row[3]);
                Date da = stringToDate(row[4]);
                resultList.add(new ItemModel(row[0], row[1], row[2], ca, da));
                Log.d("log:", row[2]);
                Log.d("log:", row[4]);

            }
        } catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: " + ex);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                throw new RuntimeException("Error while closing input stream: " + e);
            }
        }
        return resultList;
    }

    // convertitori tipo dati. dallo stream csv alla lista oggetti ItemModel
    public Date stringToDate(String dataString) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        try {
            d = sdf.parse(dataString);
        } catch (ParseException ex) {
            Log.v("Exception", ex.getLocalizedMessage());
        }
        sdf.applyPattern("dd.MMM.yy HH:mm");
        sdf.format(d);
        return d;
    }

    public Double stringToDouble(String doubleString) {
        try {
            c = Double.parseDouble(doubleString);
        } catch (NumberFormatException e) {
            throw new RuntimeException("Error while formatting input stream: from String to Double after csv import " + e);

        }
        return c;
    }
}