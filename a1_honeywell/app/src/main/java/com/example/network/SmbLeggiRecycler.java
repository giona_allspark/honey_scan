package com.example.network;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.swatch.R;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.LinkedList;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;

//  ______________Leggi risultati su recyclerview__________________
public class SmbLeggiRecycler extends AppCompatActivity {
    // Unique tag required for the intent extra
    public static final String EXTRA_MESSAGE="com.example.network.extra.FILENAME";
    public final String FILE_PATH = "smb://10.10.10.68/Users/giona/zzz/";
    public static String FILE_NAME;
    // lista oggetti per recyclerview
    private final LinkedList<Smb_Model> mSmbList = new LinkedList<>();
    // lista file in cartella condivisa
    private final LinkedList<String> list = new LinkedList<>();

    public NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication("ALLSPARK1", "Administrator", "Allspark1");
    // UI input
    static Button bt1;
    public RecyclerView mRView;
    public SmbListAdapter mAdapter;

        String sPC = "smb://10.10.10.68/Users/giona/zzz/";
        String sSD = Environment.getExternalStorageDirectory().toString();

        // String fileName = "gigi.csv";
        String pcFile;// = (sPC + FILE_NAME);
        String filedest;// = (sSD + "/" + FILE_NAME);
        File f;// = new File(filedest);


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.smb_activity);
        bt1 = findViewById(R.id.SmButton);
        bt1.setEnabled(true);

        caricaLista();

    }

    public void press_bt1(View view) throws MalformedURLException, SmbException, IOException {
        bt1.setEnabled(false);
        ReadServer rr = new ReadServer();
        rr.execute("");

    }

    public void fillData() {
        mSmbList.clear();
        for (int i = 0; i < list.size(); i++) {
            mSmbList.add(new Smb_Model(list.get(i).toString()));
            Log.d("▒ ......... ▒", (list.get(i).toString()));
        }
    }

    public void caricaLista() {
        // PUNTATORE:  Get a handle to the RecyclerView.
        RecyclerView rView = findViewById(R.id.leggiSmbView);
        // Create an adapter and supply the data to be displayed.
        mAdapter = new SmbListAdapter(this, mSmbList);
        // Connect the adapter with the RecyclerView.
        rView.setAdapter(mAdapter);
        // Give the RecyclerView a default layout manager.
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rView.setLayoutManager(llm);

        // interazione con la lista
        ItemTouchHelper helper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(0,
                        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                    @Override
                    public boolean onMove(RecyclerView recyclerView,
                                          RecyclerView.ViewHolder viewHolder,
                                          RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                        int position = viewHolder.getAdapterPosition();
                        Smb_Model mSmbList = mAdapter.getSmbPosition(position);

                        FILE_NAME = mSmbList.getFileName();
                        pcFile = (sPC + FILE_NAME);
                        filedest = (sSD + "/" + FILE_NAME);
                        f = new File(filedest);

                        // copia il file nel telefono e ottieni la dir
                        OnPhoneFromServer cc = new OnPhoneFromServer();
                        cc.execute("");

                    }
                    @Override
                    public boolean isLongPressDragEnabled() {
                        return false;
                    }
                });

        helper.attachToRecyclerView(rView);
        bt1.setEnabled(true);
    }

    // _____ adapter per il recyclerview
    public class SmbListAdapter extends RecyclerView.Adapter<SmbListAdapter.SmbViewHolder>{

        // view holder
        // crea il viewHolder - contenente tutte le sotto-view della riga
        public class SmbViewHolder extends RecyclerView.ViewHolder {
            public TextView smbFile1;

            // costruttore con tutte le sotto-view
            public SmbViewHolder(View itemView) {
                // Stores the itemView in a public final member variable that can be used
                // to access the context from any ViewHolder instance.
                super(itemView);
                smbFile1 = (TextView) itemView.findViewById(R.id.text_smb_1);
            }
        }
// view holder _

// fill the adapter

        private LinkedList<Smb_Model> mSmbList;// hold data in the adapter (cached copy)
        private LayoutInflater mInflater;

        // costruttore che inizializza gli elementi(item). legge xml e li rende view
        //  public ItemListAdapter(MainActivity mainActivity, LinkedList<ItemModel> mItemList) {
        //this.mItemList = mItemList;
        //   }
        SmbListAdapter(Context context, LinkedList<Smb_Model> mSmbList) {
            mInflater = LayoutInflater.from(context);
            this.mSmbList = mSmbList;
        }


        @Override //riempie il layout componente e crea l'holder col layout e adapter.
        public SmbViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            Context context = parent.getContext();
            LayoutInflater mInflater = LayoutInflater.from(context);
            // riempi il layout riga
            View smbView = mInflater.inflate(R.layout.smb_item, parent, false);
            // istanzia l'holder
            SmbViewHolder vhold = new SmbViewHolder(smbView);
            return vhold;
        }

        @Override // connette i dati con le view attraverso l'holder
        public void onBindViewHolder(@NonNull SmbListAdapter.SmbViewHolder vhold, int position) {
            // ottieni il modello dati ordinati per posizione
            Smb_Model mCurrent;
            mCurrent = mSmbList.get(position);
            TextView tt1 = vhold.smbFile1;
            tt1.setText(mCurrent.getFileName());
        }

        public void setItem(LinkedList<Smb_Model> a) {
            mSmbList = a;
            notifyDataSetChanged();
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView mRecyclerView) {
            super.onAttachedToRecyclerView(mRecyclerView);
        }

        // getItemCount() is called many times, and when it is first called,
        // mItemList has not been updated (means initially, it's null, and we can't return null).
        @Override
        public int getItemCount() {
            if (mSmbList != null) return mSmbList.size(); // list size
            else return 0;
        }
        // trova la posizione di un art sullo schermo (per swipe)
        public Smb_Model getSmbPosition(int position){
            return mSmbList.get(position);
        }
    }

    // ___________model per l'adapter
    public class Smb_Model extends LinkedList {
        String fileName;

        public Smb_Model(String fileName) {
            this.fileName = fileName;
        }

        public String getFileName() {
            return fileName;
        }

    }

    // legge elenco file in cartella remota (con client samba)
    public class ReadServer extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(String r) {
            fillData();
            caricaLista();

        }

        // legge sulle cartelle condivise nel server
        @Override
        protected String doInBackground(String... params) {

            list.clear();

            String url = FILE_PATH;  //"smb://yourhost/yourpath/";

            SmbFile dir = null;
            String b = "";

            try {
                dir = new SmbFile(url, auth);
                b = "tried";
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                assert dir != null;
                for (SmbFile f : dir.listFiles()) {
                    String g = f.getName();
                    list.add(g);
                    Log.d("▒▒▒▒▒▒▒▒", g);

                }
            } catch (SmbException e) {
                e.printStackTrace();
            }
            String a = dir.toString();
            Log.d("▒ ▒ ▒ ▒ ▒", a);
            return a;
        }
    }


    public class OnPhoneFromServer extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(String r) {
            Intent intent = new Intent(SmbLeggiRecycler.this, ItemLeggiRecycler.class);
            intent.putExtra(EXTRA_MESSAGE, f.toString());
            startActivity(intent);
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            SmbFile file = null;
            byte[] buffer = new byte[1024];
            try {
                String url = pcFile;
                file = new SmbFile(url, auth);
                FileOutputStream outputStream = null;  //stream of byte
                StringBuilder sb = new StringBuilder();

                //                  reader = coded text,   <--      ascii     <--           samba input
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(new SmbFileInputStream(file)))) {
                    String line;

                    while ((line = reader.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                    reader.close();
                    String url1 = filedest;

                    try (BufferedWriter writer = new BufferedWriter(new FileWriter(url1))) {
                        writer.append(sb);//internamente fa .toString();
                        writer.flush();
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return "ok";
        }
    }

}