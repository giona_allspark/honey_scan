package com.example.network;

import com.example.swatch.Listino;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.LinkedList;

public class ItemModel extends LinkedList {

    String codNeg;
    String barcode;
    String description;
    Double price;
    Date date;

    public ItemModel(String codNeg, String barcode, String description, Double price, Date date) {
        this.codNeg = codNeg;
        this.barcode = barcode;
        this.description = description;
        this.price = price;
        this.date = date;
    }

    public String getCodNeg() {
        return codNeg;
    }

    public String getBarcode() {
        return barcode;
    }

    public String getDescription() {
        return description;
    }

    public Double getPrice() {
        return price;
    }

    public Date getDate() {
        return date;
    }

    /**
     * public String getEntity(String codNeg, String barcode, String description, Double price, Date date) {
     * this.codNeg = codNeg;
     * this.barcode = barcode;
     * this.description = description;
     * this.price = price;
     * this.date = date;
     * String itemString = (codNeg + " | " + barcode + " | " + description + " | " + price.toString() +" | " + date.toString());
     * return itemString;
     * <p>
     * }
     */

    public static LinkedList<ItemModel> createItemsList(File f) {

        LinkedList<ItemModel> items = new LinkedList<ItemModel>();
        items.clear();

        // final String SAMPLE_CSV_FILE_PATH = "/sdcard/csvfile.csv";

        try {
            FileInputStream is;
            BufferedReader reader;
/**
            String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath();
            String fileName = "Lis_tini.csv";
            File f = new File(baseDir + File.separator + fileName);
*/
            if (f.exists()) {
                is = new FileInputStream(f);
                //  reader = new BufferedReader(new InputStreamReader(is));
                ItemCSVFile csvfile = new ItemCSVFile(is);
                items = csvfile.read();

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return items;
    }


}


