package com.example.network;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.swatch.R;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.LinkedList;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;

// samba client esempio. classe di prova, non coinvolta nel progetto.

public class SmbClientSimple extends AppCompatActivity {
    static final LinkedList<String> list = new LinkedList<>();

    static Button bt;
    static Button bt2;
    static Button bt3;
    static Button bt4;
    TextView txtinfo;
    TextView txtwrite;

    // autenticazione al server. se non richiesta usare (null, null, null)
    public NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication("ALLSPARK1", "Administrator", "Allspark1");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.smb_simple_activity);

        bt = findViewById(R.id.SmSimpleButton);
        bt2 = findViewById(R.id.button_copy_dir);
        bt3 = findViewById(R.id.button_copy_far_dir);
        bt4 = findViewById(R.id.button_write_far);
        txtinfo = findViewById(R.id.txtinfo);
        txtwrite = findViewById(R.id.textView_writeOnPc);

    }

    // legge elenco file in cartella remota condivisa
    public void press(View view) throws MalformedURLException, SmbException {
        ReadServer aa = new ReadServer();
        aa.execute("");
        bt.setEnabled(false);
    }
    // copia files tra cartelle remote
    public void press2(View view) throws MalformedURLException, SmbException {
        CopyOnServer bb = new CopyOnServer();
        bb.execute("");
        bt2.setEnabled(false);
    }
    // copia file, da dir remota a telefono
    public void press3(View view) throws MalformedURLException, SmbException {
        StreamOnPhoneFromServer cc = new StreamOnPhoneFromServer();
        cc.execute("");
        bt3.setEnabled(false);
    }
    // scrive file, da telefono a dir remota
    public void press4(View view) throws MalformedURLException, SmbException {
        WriteFromPhonetoServer dd = new WriteFromPhonetoServer();
        dd.execute("");
        bt4.setEnabled(false);
    }

    // legge elenco file in cartella remota condivisa
    private class ReadServer extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(String r) {
            bt.setEnabled(true);
            txtinfo.setText(list.toString());
        }

        // legge sulle cartelle condivise nel server
        @Override
        protected String doInBackground(String... params) {
            list.clear();
            String b = "";

            String url = "smb://10.10.10.68/Users/giona/zzz/"; //   //"smb://yourhost/yourpath/";

            SmbFile dir = null;

            try {
                dir = new SmbFile(url, auth);
                b = "tried";
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                assert dir != null;
                for (SmbFile f : dir.listFiles()) {
                    String g = f.getName();
                    list.add(g);
                    Log.d("▒▒▒▒▒▒▒▒", g);

                }
            } catch (SmbException e) {
                e.printStackTrace();
            }
            String a = dir.toString();
            Log.d("▒ ▒ ▒ ▒ ▒", a);
            return a;
        }
    }

    // copia files tra cartelle remote
    private class CopyOnServer extends AsyncTask<String, String, String> {

        String in_Dir = "smb://10.10.10.68/Users/giona/zzz/";
        String outDir = "smb://10.10.10.68/Users/giona/vvv/";

        String b = ("copied to " + outDir);

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(String r) {
            Toast.makeText(getApplicationContext(), b, Toast.LENGTH_LONG).show();
            bt2.setEnabled(true);
        }

        @Override
        protected String doInBackground(String... params) {

            SmbFile current_Folder = null;

            try {
                current_Folder = new SmbFile(in_Dir, auth);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            SmbFile target_Folder = null;

            try {
                target_Folder = new SmbFile(outDir, auth);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                current_Folder.copyTo(target_Folder);  // copia!

                return b; //false;
            } catch (Exception e) {
                return b;// false;
            }
        }

    }

    // copia file, da dir remota a telefono
    private class StreamOnPhoneFromServer extends AsyncTask<String, String, String> {
        String sPC = "smb://10.10.10.68/Users/giona/zzz/";
        String sSD = Environment.getExternalStorageDirectory().toString();

        //to remove
        String fileName = "gigi.csv";
        String pcFile = (sPC + fileName);
        String filedest = (sSD + "/" + fileName);

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(String r) {
            Toast.makeText(getApplicationContext(), sSD, Toast.LENGTH_LONG).show();
            bt3.setEnabled(true);
        }

        @Override
        protected String doInBackground(String... params) {

            SmbFile file = null;
            byte[] buffer = new byte[1024];
            try {
                String url = pcFile;
                file = new SmbFile(url, auth);
                FileOutputStream outputStream = null;  //stream of byte
                StringBuilder sb = new StringBuilder();

                //                  reader = coded text,   <--      ascii     <--           samba input
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(new SmbFileInputStream(file)))) {
                    String line;

                    while ((line = reader.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                    reader.close();
                    String url1 = filedest;

                    try (BufferedWriter writer = new BufferedWriter(new FileWriter(url1))) {
                        writer.append(sb);//internamente fa .toString();
                        writer.flush();
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            return "ok";
        }

    }

    // scrive file, da telefono a dir remota
    private class WriteFromPhonetoServer extends AsyncTask<String, String, String> {
        String pcdir = "smb://10.10.10.68/Users/giona/zzz/";
        String fileName = "scritto_Da_Cell.csv";
        String pcFile = (pcdir + fileName);
        String text = txtwrite.getText().toString();

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(String r) {
            bt4.setEnabled(true);
        }

        // legge sulle cartelle condivise nel server
        @Override
        protected String doInBackground(String... params) {

            try {

                SmbFile file = null;
                try {

                    file = new SmbFile(pcFile, auth);

                    SmbFileOutputStream out = new SmbFileOutputStream(file);
                    out.write(text.getBytes());
                    out.flush();//get all
                    out.close();

                } catch (Exception e) {
                    e.printStackTrace();
                    return "no";
                }

                return "yes";
            } catch (Exception e) {
                e.printStackTrace();
                return "no";
            }


        }
    }

}




