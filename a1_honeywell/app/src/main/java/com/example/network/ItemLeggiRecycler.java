package com.example.network;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.example.swatch.Listino;
import com.example.swatch.ListinoRepository;
import com.example.swatch.R;
import com.example.swatch.ViewModel;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
// con ItemListAdapter e ItemModel crea RecyclerView da csv ottenuto da ItemCSVFile

public class ItemLeggiRecycler extends AppCompatActivity {
    // hold data in the adapter (cached copy) dichiarata anche nell'ItemListAdapter.
    final LinkedList<ItemModel> mItemList = new LinkedList<>();

    // UI input
    public RecyclerView mRecyclerView;
    public ItemListAdapter mAdapter;
    public File fileName;
    public Button mButton;// invia listino a db
    private ViewModel mViewModel;
    private ListinoRepository mLrepo;

    String codNeg;
    String barcode;
    String description;
    Double price;
    Date date;


/**
    public String filename = "";//"data/data/SalvaProva/files/miofile.txt";
    //static String csvFile = "src/main/resources/csvfile.csv"
    File dir = Environment.getExternalStorageDirectory();
    File f = new File(dir, filename);
    String file = f.toString();
    //file = new File(file.getAbsolutePath());
 */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leggi_activity);
        // tentativo di passare stringa file remoto da smb
        Intent intent = getIntent();
        String message = intent.getStringExtra(SmbLeggiRecycler.EXTRA_MESSAGE);
        fileName = new File(message);

        fillData();
        caricaLista();

        // bottone 'invia listino'
        mButton = findViewById(R.id.leggi);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //converte itemModel in listino per inserirlo in DB
                ItemModel a = new ItemModel(codNeg, barcode, description, price, date);

                for (int num = 0; num < mItemList.size(); num++)

                    a =(mItemList.get(num));
                String ca = a.getCodNeg();
                String cb = a.getBarcode();
                String cc = a.getDescription();
                Double cd = a.getPrice();
                Date ce = a.getDate();
                Listino b = new Listino(ca, cb,cc, cd, ce);

                mViewModel.insertListino(b);

            }
        });
    }

    // crea lista di oggetti ItemModel (listini)
    public void fillData() {
        mItemList.clear();
        LinkedList<ItemModel> a = ItemModel.createItemsList(fileName);// in costruzione:assegnazione file <_<_<
        mItemList.addAll(a);//ItemModel.createItemsList());
        // Notify the adapter, that the data has changed.
        // mRecyclerView.getAdapter().notifyItemInserted(dim);
        // mRecyclerView.smoothScrollToPosition(dim);
    }

    // carica lista listini nel recyclerview
    public void caricaLista() {
        // PUNTATORE:  Get a handle to the RecyclerView.
        RecyclerView mRecyclerView = findViewById(R.id.leggiView);
        // Create an adapter and supply the data to be displayed.
        mAdapter = new ItemListAdapter(this, mItemList);
        // Connect the adapter with the RecyclerView.
        mRecyclerView.setAdapter(mAdapter);
        // Give the RecyclerView a default layout manager.
        LinearLayoutManager llm = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(llm);
    }

}

