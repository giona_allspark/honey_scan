package com.example.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.swatch.R;

import java.util.LinkedList;

public class ItemListAdapter extends
        RecyclerView.Adapter<ItemListAdapter.ItemViewHolder>  {

// view holder
    // crea il viewHolder - contenente tutte le sotto-view della riga
    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView tv1;
        public TextView tv2;
        public TextView tv3;
        public TextView tv4;
        public TextView tv5;

        // costruttore con tutte le sotto-view
        public ItemViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);
            tv1 = (TextView) itemView.findViewById(R.id.textView1);
            tv2 = (TextView) itemView.findViewById(R.id.textView2);
            tv3 = (TextView) itemView.findViewById(R.id.textView3);
            tv4 = (TextView) itemView.findViewById(R.id.textView4);
            tv5 = (TextView) itemView.findViewById(R.id.textView5);
        }
    }
// view holder _

// fill the adapter

    private LinkedList<ItemModel> mItemList;// hold data in the adapter (cached copy)
    private LayoutInflater mInflater;
    // costruttore che inizializza gli elementi(item). legge xml e li rende view
  //  public ItemListAdapter(MainActivity mainActivity, LinkedList<ItemModel> mItemList) {
         //this.mItemList = mItemList;
 //   }
    ItemListAdapter(Context context, LinkedList<ItemModel> mItemList) {
        mInflater = LayoutInflater.from(context);
        this.mItemList = mItemList;
    }


    @Override //riempie il layout componente e crea l'holder col layout e adapter.
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater mInflater = LayoutInflater.from(context);
        // riempi il layout riga
        View itemView = mInflater.inflate(R.layout.list_item, parent, false);
        // istanzia l'holder
        ItemViewHolder vhold = new ItemViewHolder(itemView);
        return vhold;
    }

    @Override // connette i dati con le view attraverso l'holder
    public void onBindViewHolder(@NonNull ItemListAdapter.ItemViewHolder vhold, int position) {
        // ottieni il modello dati ordinati per posizione
        ItemModel mCurrent;
        mCurrent = mItemList.get(position);
        TextView tt1 = vhold.tv1;
        tt1.setText(mCurrent.getCodNeg());
        TextView tt2 = vhold.tv2;
        tt2.setText(mCurrent.getBarcode());
        TextView tt3 = vhold.tv3;
        tt3.setText(mCurrent.getDescription());
        TextView tt4 = vhold.tv4;
        tt4.setText((String)mCurrent.getPrice().toString());
        TextView tt5 = vhold.tv5;
        tt5.setText((String) mCurrent.getDate().toString());

    }

    public void setItem(LinkedList<ItemModel> a){
        mItemList = a;
        notifyDataSetChanged();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView mRecyclerView) {
        super.onAttachedToRecyclerView(mRecyclerView);
    }

    // getItemCount() is called many times, and when it is first called,
    // mItemList has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mItemList != null)
            return mItemList.size(); // list size
        else return 0;
    }

}
