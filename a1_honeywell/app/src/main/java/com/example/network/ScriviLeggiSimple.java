package com.example.network;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.swatch.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

// samba client esempio. classe di prova, non coinvolta nel progetto.

public class ScriviLeggiSimple extends AppCompatActivity {
    final String SAMPLE_CSV_FILE_PATH = "/sdcard/csvfile.csv";

    // UI input
    public EditText testoScrivi;
    public TextView testoLeggi;


    public String filename = "csvfile.csv";//"data/data/SalvaProva/files/miofile.txt";

    //static String csvFile = "src/main/resources/csvfile.csv"
    File dir = Environment.getExternalStorageDirectory();
    File f = new File(dir, filename);
    String file = f.toString();
    //file = new File(file.getAbsolutePath());

    public void iniZialiZZa() {
        testoScrivi = findViewById(R.id.editText);//ID elemento
        testoLeggi = findViewById(R.id.textView);//ID elemento
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scrivi_leggi_csv_simple);
        iniZialiZZa();
    }

    public void leggi_Button_click(View view) {
        leggiStream();
    }

    public void scrivi_Button_click(View view) {
        scriviStream();
    }

    public void scriviStream() { // scrive su file il testo di input in textView
        String testo = testoScrivi.getText().toString(); // testo

        FileOutputStream outputStream = null;
        try {
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);// MODE_APPEND
            outputStream.write(testo.getBytes());
            outputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void leggiStream() {  // legge file csv (scritto da riga textView)
        try {
            byte[] bytes = new byte[1024];

            FileInputStream in = openFileInput(filename);
            in.read(bytes);
            in.close();

            String string = new String(bytes);
            testoLeggi.setText(string);

            InputStreamReader inputStreamReader = new InputStreamReader(in);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//non usato
    public void leggiCSV() {

        try {
            FileInputStream is;
            BufferedReader reader;
            final File file = new File(SAMPLE_CSV_FILE_PATH);

            if (file.exists()) {
                is = new FileInputStream(file);
                reader = new BufferedReader(new InputStreamReader(is));
                ItemCSVFile csvfile = new ItemCSVFile(is);

                LinkedList<ItemModel> mItemList = csvfile.read();
                //mItemList = csvfile.read();

                Log.d("main: ..  ", String.valueOf(mItemList));

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
// non usato
    public void csv2() {
        try {
            FileInputStream is;
            BufferedReader reader;
            final File file = new File(SAMPLE_CSV_FILE_PATH);

            if (file.exists()) {
                is = new FileInputStream(file);
                reader = new BufferedReader(new InputStreamReader(is));
                ItemCSVFile csvfile = new ItemCSVFile(is);
                List a = csvfile.read();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}


