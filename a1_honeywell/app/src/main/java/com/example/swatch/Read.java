package com.example.swatch;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

// ### honeywell import

import com.honeywell.aidc.AidcManager;
import com.honeywell.aidc.BarcodeReader;

import java.util.List;

public class Read extends AppCompatActivity {
    public static final int NEW_ART_ACTIVITY_REQUEST_CODE = 1;

    // conserva i dati delle view oltre i cicli delle activity
    private ViewModel mViewModel;

    // ### honeywell
    private static BarcodeReader barcodeReader;  // oggetto scanner
    private AidcManager manager;// singleton per creare barcodeReader (.create)
    // honeywell ###
    // menu

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

             // fissa l'orientamento verticale per non attivare l'onDestroy
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // create the AidcManager providing a Context and a CreatedCallback implementation.
        AidcManager.create(this, new AidcManager.CreatedCallback() {

            @Override
            public void onCreated(AidcManager aidcManager) {
                manager = aidcManager;
                try {
                    barcodeReader = manager.createBarcodeReader();
                } catch (Exception e) {
                    Toast.makeText(Read.this, "Exception: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        //comando per avviare la scansione dei codici
       // Intent barcodeIntent = new Intent("android.intent.action.CLIENTBARCODEACTIVITY");
      //  startActivity(barcodeIntent);

        FloatingActionButton fab = findViewById(R.id.fab);  // pulsante 'aggiungi'  (+)
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Read.this, Scan.class);
                startActivityForResult(intent, NEW_ART_ACTIVITY_REQUEST_CODE);;
            }
        });

        // aggiungi il recyclerView (ListinoListAdapter) schermo a scorrimento
        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final ArtListAdapter adapter = new ArtListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //  conserva i dati delle view oltre i cicli delle activity
        mViewModel = ViewModelProviders.of(this).get(ViewModel.class);
        // Observer.
        // Mentre L'activity è in foreground, aggiorna i dati nella chache dell'adapter.
        mViewModel.getAllArt().observe(this, new Observer<List<Art>>() {
            @Override
            public void onChanged(@Nullable final List<Art> art) {
                // update la copia cache dei dati nell'adapter
                adapter.setArt(art);
            }
        });
        // swipe items in the recycler view to delete it
        ItemTouchHelper helper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(0,
                        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView,
                                          RecyclerView.ViewHolder viewHolder,
                                          RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder,
                                         int direction) {
                        int position = viewHolder.getAdapterPosition();
                        Art myArt = adapter.getArtPosition(position);
                        Toast.makeText(Read.this, "Deleting " +
                                myArt.getArt(), Toast.LENGTH_LONG).show();
                        // Delete the art
                        mViewModel.deleteArt(myArt);
                    }
                });
        helper.attachToRecyclerView(recyclerView);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;}
        if (id == R.id.clear_data) { // delete all
            // Add a toast just for confirmation
            Toast.makeText(this, "Deleting All art..",Toast.LENGTH_SHORT).show();
            mViewModel.deleteAllArt();
            return true;
        }
        if (id == R.id.clear_listino) { // delete all
            // Add a toast just for confirmation
            Toast.makeText(this, "Deleting All listini..",Toast.LENGTH_SHORT).show();
            mViewModel.deleteAllListino();
            return true;
        }

        return super.onOptionsItemSelected(item);
    } // menu

    static BarcodeReader getBarcodeObject() {  // ### honeywell
        return barcodeReader;
    } // getter per altre classi###


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_ART_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Art art = new Art(data.getStringExtra(Scan.EXTRA_BAR),
                    data.getStringExtra(Scan.EXTRA_SER),
                    data.getStringExtra(Scan.EXTRA_ZON));
            mViewModel.insertArt(art);

        } else {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }
    // ### honeywell
    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (barcodeReader != null) {
            // close BarcodeReader to clean up resources.
            barcodeReader.close();
            barcodeReader = null;
        }

        if (manager != null) {
            // close AidcManager to disconnect from the scanner service.
            // once closed, the object can no longer be used.
            manager.close();
        }
    }
    // honeywell ###
}
