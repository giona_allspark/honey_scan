package com.example.swatch;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.network.SmbLeggiRecycler;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // fissa l'orientamento verticale per non attivare l'onDestroy
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void button_scan(View view){
        Intent intent = new Intent(MainActivity.this, Scan.class);
        startActivity(intent);
    }

    public void button_list(View view){
        Intent intent = new Intent();
        // call intent from different package :)
        intent = new Intent(this, com.example.network.SmbLeggiRecycler.class);
        startActivity(intent);
    }





}
