package com.example.swatch;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;
// ViewModel è un'interfaccia tra UI e repository,
// mantiene e processa i dati tenendo UI e repo separati.
// Può così porsi un observer sui dati in transito
// e aggiornare UI solo al loro variare.
// DB e View sono scollegati e meglio testabili.

public class ViewModel extends AndroidViewModel {
    // member var con il repository
    private ArtRepository mArtRepository;
    private ListinoRepository mListinoRepository;
    // member var LiveData per cache lista DB
    private LiveData<List<Art>> mAllArt;
    private LiveData<List<Listino>> mAllListino;

    //costruttore con ref al repo da cui ottiene la lista dal db
    public ViewModel(Application application) {
        super(application);
        mArtRepository = new ArtRepository(application);
        mAllArt = mArtRepository.getAllArt();
        mAllListino = mListinoRepository.getAllListino();
    }
    // get all data - senza coinvolgere UI
    LiveData<List<Art>> getAllArt() {return mAllArt;}
    LiveData<List<Listino>> getAllListino() {return mAllListino;}

    // wrapper che chiama insert dal repo. senza coinvolgere UI
    public void insertArt(Art art) {mArtRepository.insertArt(art); }
    public void deleteAllArt() {mArtRepository.deleteAllArt(); }
    public void deleteArt(Art art) {mArtRepository.deleteArt(art); }

    public void insertListino(Listino listino) {mListinoRepository.insertListino(listino); }
    public void deleteAllListino() {mListinoRepository.deleteAllListino(); }
    public void deleteListino(Listino listino) {mListinoRepository.deleteListino(listino); }

}
