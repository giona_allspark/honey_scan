package com.example.swatch;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

// Fornisce API per operare sui dati, permettendo l’uso di diverse fonti.
public class ListinoRepository {

    private ListinoDao mListinoDao;
    private LiveData<List<Listino>> mAllListino;

    // richiama DB e inizializza var membri
    ListinoRepository(Application application) {
        DbRoom db = DbRoom.getDb(application);   // ottieni db
        mListinoDao = db.listinoModel();      // associa listinoModel (listinoDao)
        mAllListino = mListinoDao.getListinoByIdOrder();    // chiamata SQL dal DAO
    }

    LiveData<List<Listino>> getAllListino() { // in thread separato
        return mAllListino;
    }

    // wrappa il metodo insert per fagli eseguire l'operazione in
    // modo asincrono o l'app crasha. Room non permette lunghi lavori su UI
    // >> insert listino
    public void insertListino (Listino listino) {
        new insertAsyncTaskL(mListinoDao).execute(listino);
    }
    private static class insertAsyncTaskL extends AsyncTask<Listino, Void, Void> {

        private ListinoDao mAsyncTaskDao;

        insertAsyncTaskL(ListinoDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Listino... params) {
            mAsyncTaskDao.insertListino(params[0]);
            return null;
        }
    }

    // >> classe Delete All listini
    private static class deleteAllListinoAsyncTask extends AsyncTask<Void, Void, Void> {
        private ListinoDao mAsyncTaskDao;

        deleteAllListinoAsyncTask(ListinoDao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAllListino();
            return null;
        }
    }
    public void deleteAllListino() {   // metodo delete all listini
        new deleteAllListinoAsyncTask(mListinoDao).execute();// classe Delete All listini
    }

    // >> delete single listino
    private static class deleteListinoAsyncTask extends AsyncTask<Listino, Void, Void> {
        private ListinoDao mAsyncTaskDao;

        deleteListinoAsyncTask(ListinoDao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final Listino... params) {
            mAsyncTaskDao.deleteListino(params[0]);
            return null;
        }
    }
    public void deleteListino(Listino listino) {   // metodo delete art
        new deleteListinoAsyncTask(mListinoDao).execute(listino);// classe delete single art.
    }

}
