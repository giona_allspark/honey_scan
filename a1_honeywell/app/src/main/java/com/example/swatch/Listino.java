package com.example.swatch;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Date;

// definisce entità. opzionale: dai nome tabella (default: Class name)
@Entity(tableName = "Listino")
public class Listino {
    // ** Setta tabella. Ogni campo pubblico o con get **

  //@ColumnInfo(name= "=/= da var name")
    @PrimaryKey(autoGenerate = true)// univoco
    public int id;

    @NonNull
    @ColumnInfo(name= "Codice Negozio")
    public String codNeg;

    @NonNull
    public String barcode;

    @NonNull
    @ColumnInfo(name= "Descrizione")
    public String description;

    @NonNull
    public Double price;

    @NonNull
    public Date date;


    public Listino(String codNeg, String barcode, String description, Double price, Date date) {
        this.codNeg = codNeg;
        this.barcode = barcode;
        this.description = description;
        this.price = price;
        this.date = date;
    }
    public String getCodNeg() {return this.codNeg;}
    public String getBarcode() {return this.barcode;}
    public String getDescription() {return this.description;}
    public Double getPrice() {return this.price;}
    public Date getDate() {return this.date;}
}