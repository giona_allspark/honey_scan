package com.example.swatch;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

// Fornisce API per operare sui dati, permettendo l’uso di diverse fonti.
public class ArtRepository {

    private ArtDao mArtDao;
    private LiveData<List<Art>> mAllArt;

    // richiama DB e inizializza var membri
    ArtRepository(Application application) {
        DbRoom db = DbRoom.getDb(application);   // ottieni db
        mArtDao = db.artModel();                // associa artModel (artDao)
        mAllArt = mArtDao.getArtByIdOrder();   // chiamata SQL dal DAO
    }

    LiveData<List<Art>> getAllArt() { // in thread separato
        return mAllArt;
    }

    // wrappa il metodo insert per fagli eseguire l'operazione in
    // modo asincrono o l'app crasha. Room non permette lunghi lavori su UI
    // >> insert
    public void insertArt (Art art) {
        new insertAsyncTask(mArtDao).execute(art);
    }
    private static class insertAsyncTask extends AsyncTask<Art, Void, Void> {

        private ArtDao mAsyncTaskDao;

        insertAsyncTask(ArtDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Art... params) {
            mAsyncTaskDao.insertArt(params[0]);
            return null;
        }
    }

    // >> classe Delete All articoli
    private static class deleteAllArtAsyncTask extends AsyncTask<Void, Void, Void> {
        private ArtDao mAsyncTaskDao;

        deleteAllArtAsyncTask(ArtDao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAllArt();
            return null;
        }
    }
    public void deleteAllArt() {   // metodo delete all art
        new deleteAllArtAsyncTask(mArtDao).execute();// classe Delete All articoli
    }

    // >> delete single art.
    private static class deleteArtAsyncTask extends AsyncTask<Art, Void, Void> {
        private ArtDao mAsyncTaskDao;

        deleteArtAsyncTask(ArtDao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final Art... params) {
            mAsyncTaskDao.deleteArt(params[0]);
            return null;
        }
    }
    public void deleteArt(Art art) {   // metodo delete art
        new deleteArtAsyncTask(mArtDao).execute(art);// classe delete single art.
    }

}
