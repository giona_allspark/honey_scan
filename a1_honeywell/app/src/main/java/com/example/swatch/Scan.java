package com.example.swatch;

import java.util.HashMap;
import java.util.Map;

import android.content.SharedPreferences;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.honeywell.aidc.*;



public class Scan extends AppCompatActivity implements BarcodeReader.BarcodeListener,
        BarcodeReader.TriggerListener {
    public static final int NEW_ART_ACTIVITY_REQUEST_CODE = 1;

    private com.honeywell.aidc.BarcodeReader barcodeReader;

    // dati extra allegati all'activity: crea costanti
    public static final String EXTRA_BAR = "com.example.swatch.BAR";
    public static final String EXTRA_SER = "com.example.swatch.SER";
    public static final String EXTRA_ZON = "com.example.swatch.ZON";

    private EditText mSerialcode;
    private EditText mBarcode;
    private EditText mZone;
    private ToggleButton swSer;

    static String zoneMem;//zona default (la reinserisco solo quando cambia)
    static Boolean serIo = true; // switch serial

    public void initializeViews() { // inizializza elementi nella view
        mSerialcode = findViewById(R.id.serialEditText);
        mBarcode = findViewById(R.id.barcodeEditText);
        mZone = findViewById(R.id.zoneEditText);
        swSer = findViewById(R.id.switch_serial);
        // setta param inizialli
        swSer.setChecked(serIo);
        mZone.setText(zoneMem);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {  // menu
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scan, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // azioni nel menu
        if (id == R.id.action_settings) {
            // chiama settings activity
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;}

        return super.onOptionsItemSelected(item);
    }

        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_art);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // setta orient. verticale, per non attuare cambiamenti nell' onDestroy()
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // inizializza elementi nella view
        initializeViews();

        String zone = mZone.getText().toString();
        String barcode = mBarcode.getText().toString();
        String serialcode = mSerialcode.getText().toString();

        // toggle serial on/off
        swSer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                serIo = swSer.isChecked();
            }
        });

        // pulsante CHANGE
        final Button buttonChange = findViewById(R.id.button_change);
        buttonChange.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                mZone.setText("");
                zoneMem=null;
            }
        });

        // pulsante RESET
        final Button buttonReset = findViewById(R.id.button_reset);
        buttonReset.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                mBarcode.setText("");
                mSerialcode.setText("");
            }
            });

        // pulsante SALVA
        final Button buttonSave = findViewById(R.id.button_save);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent saveIntent = new Intent();

                // non salvare se zone o bar vuoti
                if((TextUtils.isEmpty(mBarcode.getText())|| TextUtils.isEmpty(mZone.getText()))) {
                    setResult(RESULT_CANCELED, saveIntent);
                }else{
                    // populate strings
                    String zone = mZone.getText().toString();
                    String barcode = mBarcode.getText().toString();
                    String serialcode = mSerialcode.getText().toString();

                    // dati extra allegati all'activity: allega dati(key, value)
                    saveIntent.putExtra(EXTRA_BAR, barcode);
                    saveIntent.putExtra(EXTRA_SER, serialcode);
                    saveIntent.putExtra(EXTRA_ZON, zone);
                    zoneMem = zone;
                    setResult(RESULT_OK, saveIntent);
                }
                finish();
            }
        });

        // get bar code instance from Read
        barcodeReader = Read.getBarcodeObject();

        if (barcodeReader != null) {

            // register bar code event listener
            barcodeReader.addBarcodeListener(this);

            // set the trigger mode to client control
            try {
                barcodeReader.setProperty(BarcodeReader.PROPERTY_TRIGGER_CONTROL_MODE,
                        BarcodeReader.TRIGGER_CONTROL_MODE_CLIENT_CONTROL);
            } catch (UnsupportedPropertyException e) {
                Toast.makeText(this, "Failed to apply properties", Toast.LENGTH_SHORT).show();
            }
            // register trigger state change listener
            barcodeReader.addTriggerListener(this);

            // inizializza le preferenze	false=non sovrascritte/true=rimangono per nuovi avvii
            android.support.v7.preference.PreferenceManager.setDefaultValues(this, R.xml.preferences, true);

            SharedPreferences sharedPref = android.support.v7.preference.PreferenceManager.getDefaultSharedPreferences(this);
            Boolean ean13 = sharedPref.getBoolean(SettingsActivity.EAN_13, true);
            Boolean qr = sharedPref.getBoolean(SettingsActivity.QR, true);
            Boolean code_128 = sharedPref.getBoolean(SettingsActivity.CODE_128, true);
            Boolean gs1_128 = sharedPref.getBoolean(SettingsActivity.GS1_128, true);
            Boolean code_39 = sharedPref.getBoolean(SettingsActivity.CODE_39, true);
            Boolean matrix = sharedPref.getBoolean(SettingsActivity.MATRIX, true);
            Boolean codabar = sharedPref.getBoolean(SettingsActivity.CODABAR, true);
            Boolean upc_a = sharedPref.getBoolean(SettingsActivity.UPC_A, true);
            Boolean aztec = sharedPref.getBoolean(SettingsActivity.AZTEC, true);
            Boolean interleaved2_5 = sharedPref.getBoolean(SettingsActivity.INTERLEAVED_25, true);
            Boolean pdf417 = sharedPref.getBoolean(SettingsActivity.PDF417, true);


            // set barcode properties
            Map<String, Object> properties = new HashMap<String, Object>();
            // Set Symbologies On/Off
            properties.put(BarcodeReader.PROPERTY_CODE_128_ENABLED, code_128);
            properties.put(BarcodeReader.PROPERTY_GS1_128_ENABLED, gs1_128);
            properties.put(BarcodeReader.PROPERTY_QR_CODE_ENABLED, qr);
            properties.put(BarcodeReader.PROPERTY_CODE_39_ENABLED, code_39);
            properties.put(BarcodeReader.PROPERTY_DATAMATRIX_ENABLED, matrix);
            properties.put(BarcodeReader.PROPERTY_UPC_A_ENABLE, upc_a);
            properties.put(BarcodeReader.PROPERTY_EAN_13_ENABLED, ean13);
            properties.put(BarcodeReader.PROPERTY_AZTEC_ENABLED, aztec);
            properties.put(BarcodeReader.PROPERTY_CODABAR_ENABLED, codabar);
            properties.put(BarcodeReader.PROPERTY_INTERLEAVED_25_ENABLED, interleaved2_5);
            properties.put(BarcodeReader.PROPERTY_PDF_417_ENABLED, pdf417);
            // Set Max Code 39 barcode length
            properties.put(BarcodeReader.PROPERTY_CODE_39_MAXIMUM_LENGTH, 10);
            // Turn on center decoding
            properties.put(BarcodeReader.PROPERTY_CENTER_DECODE, true);
            // Disable bad read response, handle in onFailureEvent
            properties.put(BarcodeReader.PROPERTY_NOTIFICATION_BAD_READ_ENABLED, false);


            // Apply the settings
            barcodeReader.setProperties(properties);
        }
    }

    @Override
    public void onBarcodeEvent(final BarcodeReadEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                String zone = mZone.getText().toString();
                String barcode = mBarcode.getText().toString();
                String serialcode = mSerialcode.getText().toString();

                String g = event.getBarcodeData();

                if (TextUtils.isEmpty(zone)) {mZone.setText(g);}
                else if (TextUtils.isEmpty(barcode)) {mBarcode.setText(g);}
                else{ mSerialcode.setText(g);}
            }
        });
    }

    @Override
    public void onTriggerEvent(TriggerStateChangeEvent event) {
        try {
            // only handle trigger presses
            // turn on/off aimer, illumination and decoding
            barcodeReader.aim(event.getState());
            barcodeReader.light(event.getState());
            barcodeReader.decode(event.getState());

        } catch (ScannerNotClaimedException e) {
            e.printStackTrace();
            Toast.makeText(this, "Scanner is not claimed", Toast.LENGTH_SHORT).show();
        } catch (ScannerUnavailableException e) {
            e.printStackTrace();
            Toast.makeText(this, "Scanner unavailable", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailureEvent(BarcodeFailureEvent arg0) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(Scan.this, "No data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (barcodeReader != null) {
            try {
                barcodeReader.claim();
            } catch (ScannerUnavailableException e) {
                e.printStackTrace();
                Toast.makeText(this, "Scanner unavailable", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (barcodeReader != null) {
            // release the scanner claim so we don't get any scanner
            // notifications while paused.
            barcodeReader.release();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (barcodeReader != null) {
            // unregister barcode event listener
            barcodeReader.removeBarcodeListener(this);

            // unregister trigger state change listener
            barcodeReader.removeTriggerListener(this);
        }
    }
}

