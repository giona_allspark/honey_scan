package com.example.swatch;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
// RecyclerView. mostra lista a scorrimento dinamico sullo schermo
public class ArtListAdapter extends RecyclerView.Adapter<ArtListAdapter.ArtViewHolder> {

    private final LayoutInflater mInflater;
    private List<Art> mArt; // Cached copy of Art

    ArtListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public ArtViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new ArtViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ArtViewHolder holder, int position) {
        if (mArt != null) {
            Art current = mArt.get(position);
            holder.zoneItemView.setText(current.getZone());
            holder.barItemView.setText(current.getArt());
            holder.serialItemView.setText(current.getSer());

        } else {
            // Covers the case of data not being ready yet.
            holder.barItemView.setText("No art.");
            holder.zoneItemView.setText("No zone");
            holder.serialItemView.setText("No serial");
        }
    }

    void setArt(List<Art> art){
        mArt = art;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mArt has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mArt != null)
            return mArt.size();
        else return 0;
    }

    class ArtViewHolder extends RecyclerView.ViewHolder {
        private final TextView zoneItemView;
        private final TextView barItemView;
        private final TextView serialItemView;


        private ArtViewHolder(View itemView) {
            super(itemView);
            zoneItemView = itemView.findViewById(R.id.zoneTextView);
            barItemView = itemView.findViewById(R.id.barTextView);
            serialItemView = itemView.findViewById(R.id.serialTextView);
        }
    }
    // trova la posizione di un art sullo schermo (per swipe)
    public Art getArtPosition(int position){
        return mArt.get(position);
    }
}
