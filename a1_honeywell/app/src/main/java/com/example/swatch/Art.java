package com.example.swatch;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

// definisce entità. opzionale: dai nome tabella (default: Class name)
@Entity(tableName = "Articoli")
public class Art {
    // ** Setta tabella. Ogni campo pubblico o con get **

  //@ColumnInfo(name= "=/= da var name")
    @PrimaryKey(autoGenerate = true)// univoco
    public int id;

    @NonNull
    public String barcode;

    public String serialcode;  // seriale (non sempre presente)

    @NonNull
    public String zone;  // reparto


    public Art(@NonNull String barcode, String serialcode, String zone) {
        this.barcode = barcode;
        this.serialcode = serialcode;
        this.zone = zone;
    }
    public String getArt() {return this.barcode;}
    public String getSer() {return this.serialcode;}
    public String getZone() {return this.zone;}
}