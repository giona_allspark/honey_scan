package com.example.swatch;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SettingsActivity extends AppCompatActivity {
    public static final String EAN_13 = "DEC_EAN13_ENABLED";
    public static final String QR = "qr";
    public static final String CODE_128 = "code_128";
    public static final String GS1_128 = "code_128";
    public static final String CODE_39 = "code_39";
    public static final String MATRIX = "matrix";
    public static final String CODABAR = "codabar";
    public static final String UPC_A = "upc_a";
    public static final String AZTEC = "aztec";
    public static final String INTERLEAVED_25 = "interleaved_25";
    public static final String PDF417 = "pdf417";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // chiama il fragment
        // \/ per classi 'compat'. se no usa getfragmentManager()
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}