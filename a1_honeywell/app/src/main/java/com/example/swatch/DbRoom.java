package com.example.swatch;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

// ____________ questo crea il DB ______________
// {entità1.class, entità2.class}, ver. db, tieni storico versioni? (opzionale)
@Database(entities = {Art.class, Listino.class}, version = 1, exportSchema = false)
@TypeConverters({DateConverter.class})
public abstract class DbRoom extends RoomDatabase {

    // definisci i DAO che lavorano col DB
    public abstract ArtDao artModel();
    public abstract ListinoDao listinoModel();
    // altre tabelle [...]

    // crea Room come singleton (singolo accesso al DB)
    private static DbRoom INSTANCE;

    public static DbRoom getDb(final Context context) {
        if (INSTANCE == null) {
            synchronized (DbRoom.class) {
                if (INSTANCE == null) {
                    // crea DB
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            DbRoom.class, "Db_1")
                            //comandi per migrazioni
                            .build(); // costruisce DB
                }
            }
        }
        return INSTANCE;
    }
}
