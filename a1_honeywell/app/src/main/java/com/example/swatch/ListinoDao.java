package com.example.swatch;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao // crea un data access object, che definisce le SQLite query(richieste)
public interface ListinoDao {

    // LiveData is a data holder class that can be observed within a given lifecycle.
    // Always holds/caches latest version of data.
    // Notifies its active observers when the data has changed.

    @Query("SELECT * from Listino ORDER BY id ASC")
    LiveData<List<Listino>> getListinoByIdOrder(); // lista listini in ordine id

    @Query("SELECT * from Listino ORDER BY barcode ASC")
    LiveData<List<Listino>> getListinoByBarcodeOrder(); // lista listini in ordine barcode

 // mi dà errore codNeg
    //   @Query("SELECT * from Listino ORDER BY codNeg ASC")
 //   LiveData<List<Listino>> getListinoBycodNegOrder(); // lista listini in ordine cod.negozio

    @Query("DELETE FROM Listino")
    void deleteAllListino(); // svuota la tabella

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertListino(Listino listino); //insert nuovo item

    @Update(onConflict = OnConflictStrategy.REPLACE) //.ABORT
    void updateData(Listino listino);

    @Delete
    void deleteListino(Listino listino);

}
