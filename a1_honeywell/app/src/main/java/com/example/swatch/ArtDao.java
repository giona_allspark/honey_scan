package com.example.swatch;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao // crea un data access object, che definisce le SQLite query(richieste)
public interface ArtDao {

    // LiveData is a data holder class that can be observed within a given lifecycle.
    // Always holds/caches latest version of data.
    // Notifies its active observers when the data has changed.

    @Query("SELECT * from Articoli ORDER BY id ASC")
    LiveData<List<Art>> getArtByIdOrder(); // lista articoli in ordine id

    @Query("SELECT * from Articoli ORDER BY barcode ASC")
    LiveData<List<Art>> getArtByBarcodeOrder(); // lista articoli in ordine barcode

    @Query("DELETE FROM Articoli")
    void deleteAllArt(); // svuota la tabella

    @Query("SELECT barcode, serialcode FROM Articoli")
    public List<BarAndSerial> getBarSerials();// lista dei barcode con serial

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertArt(Art art); //insert nuovo item

    @Update(onConflict = OnConflictStrategy.REPLACE) //.ABORT
    void updateData(Art art);

    @Delete
    void deleteArt(Art art);

}
