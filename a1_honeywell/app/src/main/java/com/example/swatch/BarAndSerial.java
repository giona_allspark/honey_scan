package com.example.swatch;

import android.arch.persistence.room.ColumnInfo;
import android.support.annotation.NonNull;

// crea generico richiamato in query ArtDao
public class BarAndSerial {
    @ColumnInfo(name ="barcode")
    public String barcode;

    @ColumnInfo(name = "serialcode")
    @NonNull
    public String serialcode;
}
